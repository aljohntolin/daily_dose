<?php
/****************************************************************
    @author : Jing Enriquez
    @date : May 26, 2014
    @description : push constants
    @Updated By: Aljohn Tolin
*****************************************************************/
/*
define('PUSHBASE', 'push_base');
define('SUBSCRIBER', 'subscriber');
define('LOGPATH','/home/dmp/scripts/preparation/omnibus/log/');
define('MAINPATH', ''); */

define('PUSHBASE', 'push_base');
define('SUBSCRIBER', 'subscriber');
define('LOGPATH', '/home/dmp/scripts/preparation/daily_dose/log/');
define('MAINPATH', '/home/dmp/scripts/preparation/daily_dose/');

?>
