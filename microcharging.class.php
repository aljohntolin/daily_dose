<?php 
ini_set('memory_limit', '-1');
class Microcharging{
  private $oDb = null;
  public function __construct($oDb = null){
    $this->oDb = $oDb;
  }
  public function updateMicroChargingTable(){
    //This function updates daily_dose_recovery
    $arrSuccMsisdn = $this->getMsisdnSuccessfullyBilled();
    $arrInsuffMsisdn = $this->getMsisdnInsuff();

   // if (IS_TESTING){
      //$arrInsuffMsisdn = $arrSuccMsisdn;// for testing; remove this in production
      //$arrSuccMsisdn = array();
   // }

    $strSucc = '';
    if (count($arrSuccMsisdn)>0){
      $arrTemp = array();
      foreach ($arrSuccMsisdn as $k => $v){
        $arrTemp[] = $v['msisdn'];
      }
      $strSucc = implode(',', $arrTemp);
    }


    /*
    1. All successfully msisdn will be deleted on micro charging table (daily_dose_recovery).
   If successful msisdn has insuff, this be deleted. Previous credit will be deleted
   */
    if ($strSucc!=''){
      $sqlDelete = "
        DELETE FROM Dmp_Transaction.`daily_dose_recovery` 
        WHERE msisdn in ({$strSucc});
      ";
      echo $sqlDelete." \n";
      $this->oDb->query($sqlDelete);
    }

    /*
    2. All msisdn insuff will be placed on micro charging table. If the msisdn insuff exist, 
    the record will be overwrite or reset.
    */
    $strInsuff = '';
    if (count($arrInsuffMsisdn)>0){
      $arrTemp = array();
      foreach ($arrInsuffMsisdn as $k => $v){
        $arrTemp[] = $v['msisdn'];
      }
      $strInsuff = implode(',', $arrTemp);
    }

    if ($strInsuff!=''){
      $sqlResetMsisdn = "
        DELETE FROM Dmp_Transaction.`daily_dose_recovery` 
        WHERE msisdn IN ({$strInsuff}) AND DATE(recovered_at) != '".date('Y-m-d')."';
      ";
      //delete all msisdn Insuff today but recovered_at NOT today
      echo $sqlResetMsisdn."\n";
      $this->oDb->query($sqlResetMsisdn);
    }

    //clean database, totally paid
    $sqlDeletePaid = "
      DELETE FROM Dmp_Transaction.`daily_dose_recovery` 
      WHERE credit_amount = recovered_amount;
    ";
    $this->oDb->query($sqlDeletePaid);    

    //after deleting all insuff today but recovered_at NOT today, insert the new insuff for microcharging.
    $arrServiceIdAmount = array(
      57228 => 5,
      57229 => 20,
      57230 => 40
      );
    $arrServiceIdSubsType = array(
      57228 => 1,
      57229 => 2,
      57230 => 3
      );    

    
    if (count($arrInsuffMsisdn) > 0){
      $arrMsisdnRecoveredUpdatedToday = $this->getMsisdnRecoveredUpdatedToday();
      $arrDontUpdate = array();
      if (count($arrMsisdnRecoveredUpdatedToday)>0){
        foreach($arrMsisdnRecoveredUpdatedToday as $k => $v){
          $arrDontUpdate[] = $v['msisdn'];
        }
      }

      foreach ($arrInsuffMsisdn as $k => $v){
        if (!in_array($v['msisdn'], $arrDontUpdate)){
          //if the insuff-msisdn already updated today, DONOT create new row.
          $v['service_id'] = '57228';
          $v['message'] = strtolower($v['message']);
          if (strpos($v['message'], 'dose30') !== false) {
            $v['service_id'] = '57230';
          }
          else if (strpos($v['message'], 'dose1') !== false) {
            $v['service_id'] = '57229';
          }
          else if (strpos($v['message'], 'dose') !== false) {
            $v['service_id'] = '57228';
          }                    
          $msisdn = $v['msisdn'];
          $subscription_type = (isset($arrServiceIdSubsType[$v['service_id']]))?$arrServiceIdSubsType[$v['service_id']]:0;;//can be determined on service_id
          $credit_amount = (isset($arrServiceIdAmount[$v['service_id']]))?$arrServiceIdAmount[$v['service_id']]:0; //can be determined on service_id
          $recovered_amount = 0;
          $sqlInserInsuffMsisdn = "
            INSERT INTO Dmp_Transaction.`daily_dose_recovery` (msisdn, subscription_type, credit_amount, recovered_amount)
            VALUES ({$msisdn}, {$subscription_type}, {$credit_amount}, {$recovered_amount})
          ";
          print_r($v);
          echo "\n";
          echo "$sqlInserInsuffMsisdn \n";
          $this->oDb->query($sqlInserInsuffMsisdn);
        }
      }
    }
  }
  public function getMsisdnSuccessfullyBilled(){
    //get all successfully billed regarless of daily, weekly, monthly
    $sql_backup = "SELECT ls.msisdn, ls.service_id from `Dmp_Transaction`.log_status_".date('md')." as ls LEFT JOIN `Dmp_Transaction`.log_out_".date('md')." as lo
        ON ls.transid = lo.transid WHERE ls.status_code = 0 AND ls.service_id IN (57228,57229,57230) 
        AND lo.keyword_id IN (0,2,3) AND date(ls.time_status) = '".date('Y-m-d')."'";
    $sql = "
      SELECT
        ls.msisdn, ls.service_id
      FROM `Dmp_Transaction`.log_status_".date('md')." AS ls 
      LEFT JOIN `Dmp_Transaction`.log_out_".date('md')." AS lo ON ls.transid = lo.transid
      WHERE ls.status_code = 0 AND ls.service_id IN (57228,57229,57230) 
      AND lo.keyword_id IN (0,2,3) 
      AND ls.msg_index = 1 AND
      lo.rcvd_transid != 'DDMICRO'
      AND ls.transid NOT LIKE '%DMP%';
    ";
    $result = $this->oDb->query($sql);
    $data = array();
    if($this->oDb->result != NULL){
        while($row = mysqli_fetch_assoc($this->oDb->result)){
            $data[] = $row;
        }
    }
    return $data;   
  }
  public function getMsisdnInsuff(){
    $sql = "
      SELECT
        MAX(id) as max_id, ls.msisdn
      FROM `Dmp_Transaction`.log_status_".date('md')." AS ls
      WHERE ls.status_code = 1 AND ls.service_id IN (57228,57229,57230)
      AND ls.msisdn != 0 AND DATE(ls.time_status) = '".date('Y-m-d')."'
      GROUP BY ls.msisdn
      ORDER BY id DESC;
    ";
    echo "Get recent log_status id Insuff \n";
    echo $sql. " \n";
    $result = $this->oDb->query($sql);
    $data = array();
    $arrLSMaxId = array();
    if($this->oDb->result != NULL){
        while($row = mysqli_fetch_assoc($this->oDb->result)){
            $data[] = $row;
            $arrLSMaxId[] = $row['max_id'];
        }
    }

    if (count($arrLSMaxId)==0) return array();

    $sql = "
        SELECT 
         ls.msisdn, li.message
        FROM `Dmp_Transaction`.log_status_".date('md')." AS ls
        LEFT JOIN `Dmp_Transaction`.log_out_".date('md')." AS lo ON ls.transid = lo.transid
        LEFT JOIN `Dmp_Transaction`.log_in_".date('md')." AS li ON li.transid = LEFT(ls.transid, CHAR_LENGTH(ls.transid)-3)    
        WHERE ls.id IN (".implode(',', $arrLSMaxId).") AND ls.msisdn != 0
        AND DATE(ls.time_status) = '".date('Y-m-d')."' AND li.message IS NOT NULL
        ";

    //get all insuff regarless of daily, weekly, monthly
    $sql_backup = "SELECT ls.msisdn, ls.service_id from `Dmp_Transaction`.log_status_".date('md')." as ls LEFT JOIN `Dmp_Transaction`.log_out_".date('md')." as lo
        ON ls.transid = lo.transid WHERE ls.status_code = 1 AND ls.service_id IN (57228,57229,57230) 
        AND lo.keyword_id IN (0,2,3) AND ls.msisdn != 0 AND date(ls.time_status) = '".date('Y-m-d')."'";
    echo "Get MSISDN Insuff \n";
    echo $sql. " \n";
    $result = $this->oDb->query($sql);
    $data = array();
    if($this->oDb->result != NULL){
        while($row = mysqli_fetch_assoc($this->oDb->result)){
            $data[] = $row;
        }
    }

    return $data;     
  }
  public function getMsisdnToBeMicroCharge($tariff_value){
    $sql = "
      SELECT
        *
      FROM 
        Dmp_Transaction.`daily_dose_recovery` ddr
      WHERE 
        ddr.credit_amount > ddr.recovered_amount
        AND
        (ddr.credit_amount - ddr.recovered_amount)  >= {$tariff_value}
    ";
    $result = $this->oDb->query($sql);
    $data = array();
    if($this->oDb->result != NULL){
        while($row = mysqli_fetch_assoc($this->oDb->result)){
            $data[] = $row;
        }
    }
    return $data;
  }
  public function getMsisdnRecoveredUpdatedToday(){
    $sql = "
      SELECT
        *
      FROM 
        Dmp_Transaction.`daily_dose_recovery` ddr
      WHERE 
        DATE(ddr.recovered_at) = '".date('Y-m-d')."';
    ";

    $result = $this->oDb->query($sql);
    $data = array();
    if($this->oDb->result != NULL){
        while($row = mysqli_fetch_assoc($this->oDb->result)){
            $data[] = $row;
        }
    }
    return $data;
  }
}
?>