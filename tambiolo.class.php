<?php 
ini_set('memory_limit', '-1');
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/db.class.php";

class tambiolo{
  private function tambioloEntryApi($msisdn, $entry_details, $shortcodeId=1){
    echo "api: Tambiolo Entry API \n ";
    echo "entry details: ".$entry_details." \n ";
    $parameters = "method=ticket&msisdn={$msisdn}&shortcode_id={$shortcodeId}&entry_details={$entry_details}";
    //$url = "http://23.253.177.140/m360tambiolo_api/?".$parameters;
    $url = "http://23.253.177.140/tambiolo_api/?".$parameters;
    
    echo "tambiolo entry api: ".$url." \n ";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //allow redirects
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return into a variable
    curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout after 60s
    $result = curl_exec($ch);
    curl_close($ch);
  }
  public function insertEntryTambiolo($msisdn = null, $entry_details="onpush", $number_entries="10"){
    //$shortcodeId = 1;
    $shortcodeId = 1976;
    for ($i=0; $i < $number_entries; $i++) {
      $this->tambioloEntryApi($msisdn, $entry_details, $shortcodeId);
    }    
  }


  public function updateDDoseEntrySummary($oDBSheena, $msisdn=null, $number_entries){
    $sqlUpdate = "
      UPDATE `Dmp_Transaction`.`daily_dose_entry_summary`
      SET total_entries = total_entries + {$number_entries}
      WHERE msisdn = '{$msisdn}';
    ";
    $sqlInsert = "
      INSERT INTO Dmp_Transaction.daily_dose_entry_summary
      (msisdn, total_entries)
      VALUES ('{$msisdn}', '{$number_entries}')
  ";
    $oDBSheena->select("Dmp_Transaction.daily_dose_entry_summary", "*", "msisdn = {$msisdn} LIMIT 1");
    $r = $oDBSheena->get_data();
    if (isset($r[0]['msisdn'])){
      $oDBSheena->query($sqlUpdate);
    }
    else{
      $oDBSheena->query($sqlInsert);
    }
  }
}
?>