<?php 

require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.config.php";
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.class.php";

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');
date_default_timezone_set("Asia/Manila");

// 5723 - daily
// 5724 - weekly
// 5725 - monthly

$date = date('Y-m-d');
echo "\nScript Starting..\n";

$push = new Push('10.176.160.187','dmp_push','sh3ena29I0','Dmp_Transaction'); // Sheena
#$push = new Push('10.178.20.75','dmp_push','gDg3mPush36g','Dmp_Transaction'); // MELANIE
echo "\nConnected to MELANIE.\n";
echo "Date: ".$date;
echo "\nQuerying push transaction..\n\n";

$reportall = array();
$reportsubscriptionAll = array();

$serviceIds = array('5723', '5724', '5725');
$push_prefix = array('99','01','02','03');

foreach($serviceIds as $sid){
	foreach($push_prefix as $pre){
		$transaction = $push->total_transaction($date,$sid,$pre);
		$billed = $push->total_billed($date,$sid,$pre);
		$error = $push->total_error($date,$sid,$pre);
		$insuff = $push->total_insuff($date,$sid,$pre);

		if($sid == 5723) { $push_type = 1; }
		else if($sid == 5724) { $push_type = 2; }
		else if($sid == 5725) { $push_type = 3; }
		else { $push_type = -1; }

		if($pre == 99) { $push_cycle = 0; }
		else if($pre == 01) { $push_cycle = 1; }
		else if($pre == 02) { $push_cycle = 2; }
		else if($pre == 03)	{ $push_cycle = 3; }
		else { $push_cycle = -1; }

		$report = array(
			'push_type'		=> $push_type,
			'push_cycle'	=> $push_cycle,
			'push_billed'	=> $billed,
			'error_insuff'	=> $insuff,
			'error_other'	=> $error,
			'total_transaction'	=> $transaction
		);
		array_push($reportall, $report);
		echo $sid."-".$pre." -> total: ".$transaction.", billed: ".$billed.", error: ".$error.", insuff: ".$insuff."\n";
	}
}
//print_r($reportall);

$push2 = new Push('10.208.152.24','dmp_push','DPus4832hSxMpLp','Daily_Dose'); // KITIN
echo "\n\nConnected to KITIN.";
echo "\nInserting push transactions..\n\n";
$date = '2015-01-07';
for($row=0; $row < 12; $row++) {

	$push2->insert_transaction_report($date,$reportall[$row]['push_type'],$reportall[$row]['push_cycle'],$reportall[$row]['push_billed'],
										$reportall[$row]['error_insuff'],$reportall[$row]['error_other'],$reportall[$row]['total_transaction']);
	echo "Row inserted -> date: ".$date.", push type: ".$reportall[$row]['push_type'].", push cycle: ".$reportall[$row]['push_cycle'].", push billed : ".
								  $reportall[$row]['push_billed'].", insuff : ".$reportall[$row]['error_insuff'].", error: ".$reportall[$row]['error_other'].
								  ", total transaction: ".$reportall[$row]['total_transaction']."\n";
}


echo "\n\nQuerying push subscription..\n\n";
$subscriptionAll = array();
$pushType = array('1','2','3');

foreach($pushType as $ptype){
	$opt_in = $push2->opt_in($date,$ptype);
	$opt_out = $push2->opt_out($date,$ptype);
	if($ptype == 1) { $a = "daily"; }
	if($ptype == 2) { $a = "weekly"; }
	if($ptype == 3) { $a = "monthly"; }
	echo "Date : ".$date.", Type: ".$a.", Opt In: ".$opt_in.", Opt Out: ".$opt_out."\n";
	$subscription = array(
		'push_type'		=> $ptype,
		'optin'			=> $opt_in,
		'optout'		=> $opt_out
	);
	array_push($subscriptionAll, $subscription);
}

echo "\n\nInserting push subscription..\n\n";
for($row=0; $row < 3; $row++) {
	$push2->insert_subscription_report($date,$subscriptionAll[$row]['push_type'],$subscriptionAll[$row]['optin'],$subscriptionAll[$row]['optout']);
	echo "Row inserted -> date: ".$date.", push type: ".$subscriptionAll[$row]['push_type'].", opt in: ".$subscriptionAll[$row]['optin'].", ".$subscriptionAll[$row]['optout']."\n";
}
echo "\n\nScript Completed.";
?>