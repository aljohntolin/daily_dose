<?php

/*
1. All successfully msisdn will be deleted on micro charging table (daily_dose_recovery).
   If successful msisdn has insuff, this be deleted.
2. All msisdn insuff will be placed on micro charging table. If the msisdn insuff exist, 
the record will be overwrite or reset.

ddose_micro_charging service_id amount_to_charge (40, 20, 5, 2.5)
service_id: 57228 => daily; 57229 => weekly; 57230 => monthly
valid amount: 1
e.g. 

tariff_id 8579
*/

require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.config.php";
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.class.php";
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/microcharging.class.php";
require_once "/home/dmp/dmp/process/engine/db_constants.php";
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');
date_default_timezone_set("Asia/Manila");
// 5723 - daily, 5724 - weekly, 5725 - monthly

define("IS_TESTING", 0); //1 for testing; 0 for production


if (!(isset($argv[1]) && in_array($argv[1], array(2.5, 5, 20, 40)))){
  echo "Invalid Tariff Value. Valid Tariff Value 2.5, 5, 20, 40 \n";
  die();
}
$tariff_value = $argv[1];

$date = exec('date');
$date = new DateTime($date);

$arrTariff = array(
    '40'  => 57230, 
    '20'  => 57229,
    '5'   => 57228,
    '2.5' => 57228 //same as above will overwrite the tariff_id later of the codes
  );
$service_id = $arrTariff[$tariff_value];

echo "\nPrepare for PUSH";
$push = new Push('10.208.152.24','dmp_push','DPus4832hSxMpLp','Daily_Dose'); // KITIN
echo "\nConnected to KITIN.";

$push2 = new Push('10.176.160.187','dmp_push','sh3ena29I0','Dmp_Transaction'); // Sheena

echo "\nConnected to DB.";

//New connection, i cannot use the existing connection coz some methods has been affected.
$push3 = new Push('10.176.160.187',GWUSER,GWPASS,'Dmp_Transaction'); // Sheena
$oMicrocharging = new Microcharging($push3);

$oMicrocharging->updateMicroChargingTable();

echo "\nChecking push base on Melanie...\n";
$push_base_count = $push2->check_push_base(PUSHBASE); 

if($push_base_count){
  echo "\nERROR: Cannot prepare, push base is not yet empty. Please check push base and try again later.\n";
  //exit;
}

//Get Push Details from Push Transactions
$push2->select(MDP_CONSTANTS::DBCRM . '.push_transaction'
      , '*' 
      ,"scheduled = 1 AND active = 1 AND pr_status = 1 AND service_id = '{$service_id}'",'LIMIT 1'); // query

$push_data = $push2->get_data(); //Put data from push transaction to $push_data array.
$push_data = $push_data[0];//Initially, The $push_data can be accessed like so: $push_data[0]['project_id']. This line will put the data out of the [0] index and then transferred to the $push_data. Now you can access the push_data like so: $push_data['project_id'];
$service_id = 57228;
if ($tariff_value == 2.5){
  $push_data['tariff_id'] = 8579;
  $push_data['keyword_id'] = 280298;//2.50
}
else if ($tariff_value == 5){
  $push_data['keyword_id'] = 280299;//5
}
else if ($tariff_value == 20){
  $push_data['keyword_id'] = 280300;//20
}
else if ($tariff_value == 40){
  $push_data['keyword_id'] = 280301;//40
}
 
print_r($push_data);
echo "\nDone getting the parameters needed from push_transaction table\n";

//Checking push_outbox And The Current Cycle The Push Process Is On
$DELIVERED_69_COUNT = $push2->get_delivered_result($service_id,$push_data['connection_id'],69,'push_outbox'); //TESTING: set to 0
$DELIVERED_7_COUNT = $push2->get_delivered_result($service_id,$push_data['connection_id'],7,'push_outbox'); // TESTING: set to 0
$COUNTER = $push2->get_push_rebill_counter($service_id); //TESTING: set to  0
$PREVCOUNTER = $COUNTER - 1;

echo "\nPush_outbox Counts and Push Cycle Count";
echo "\nDELIVER 7 COUNT: ".$DELIVERED_7_COUNT;
echo "\nDELIVER 69 COUNT: ".$DELIVERED_69_COUNT;
echo "\nCOUNTER COUNT: ".$COUNTER;
echo "\nPREVCOUNTER COUNT: ".$PREVCOUNTER;  
//End Checking push_outbox And The Current Cycle The Push Process Is On

function strSqlInsert($counter, $service_id, $push_data){
      $counter = str_pad($counter, 2, "0", STR_PAD_LEFT);
      $insert = "INSERT INTO " . MDP_CONSTANTS::DBCRM . "." . "push_outbox 
      (
        transid,
        msisdn,
        project_id,
        service_id,
        tariff_id,
        billing,
        connection_id,
        shortcode_id,
        source,
        keyword_id,
        alias,
        content_id,
        scene_id,
        delivered,
        ptype,
        ctype,
        mms,
        drm,
        telco_id,
        msg_index,
        rcvd_transid,
        dcs,
        content
      )

      SELECT 
      CONCAT('{$counter}{$service_id}',msisdn,UNIX_TIMESTAMP(NOW()),FLOOR(10 + (RAND() * 90)),'111') AS transid, 
      msisdn,
      '{$push_data['project_id']}' AS project_id,
      {$service_id},
      {$push_data['tariff_id']} AS tariff_id,
      '{$push_data['billing']}',
      '{$push_data['connection_id']}', 
      '{$push_data['shortcode_id']}', 
      '{$push_data['source']}',
      '{$push_data['keyword_id']}',
      '{$push_data['alias']}', 
      '{$push_data['content_id']}', 
      '{$push_data['scene_id']}',
      '77', 
      1,
      '{$push_data['ctype']}', 
      '{$push_data['mms']}', 
      '{$push_data['drm']}', 
      '{$push_data['telco_id']}', 
      '{$push_data['msg_index']}',
      'DDMICRO', 
      '{$push_data['dcs']}', 
      ''
      FROM " . MDP_CONSTANTS::DBCRM . "." . "push_base
      WHERE msisdn = 9154631489  # put this line to query above for TESTING
      ";
      
      return $insert;
}
//Validationg push_outbox content.
//if($DELIVERED_7_COUNT == 0 && $DELIVERED_69_COUNT ==0){
//$COUNTER = 1;
if(true){
  if($COUNTER <= 0) {
    echo "Please execute the regular push before executing micro charging";
    die();
  }//if($COUNTER <= 0) {
  else { //REBILL

    if ($COUNTER < 6) {
      $lsTransid = "0{$PREVCOUNTER}";
      echo "\nStart MicroCharging : ".$COUNTER;
      echo "\nInserting Push Base..";
      $arrSubs = $oMicrocharging->getMsisdnToBeMicroCharge($tariff_value);
      foreach($arrSubs as $v) {
        $push2->query("INSERT INTO ".MDP_CONSTANTS::DBCRM . "." . "push_base (msisdn) VALUES('".$v['msisdn']."')");
      }
      echo "Inserted to PUSH BASE.\n";
      echo "Inserted to Push Outbox.\n";
      $insert = strSqlInsert($COUNTER, $service_id, $push_data);
      //echo "\n";
      echo $insert."\n";
      $push2->query($insert);
    }
    else {
      echo "Maximum only of 5 microcharging.\n";
    }
    echo "\nInserted to Push Outbox.\n";
  }//else { //REBILL
}
else {
  echo "\nDelivered 7 and 69 is not yet ZERO.";
  exit;
}

echo "Completed.";

if (!IS_TESTING){//dont execute if testing
  if($push2->push_base_truncate(PUSHBASE)){
    echo "\nPUSH BASE TRUNCATED!!!\n";
  }else{
    echo "\n\n ERROR: truncate not completed. mysql_error ....\n\n";
    exit;
  }
}
