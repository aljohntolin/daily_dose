<?php

/* DB Connection Class */

class DB_Connect{
    public $dbconnection;
    public $result;
    public $total;



    public function __construct($host = '', $user = '', $pass = '', $dbname = ''){

        $this->host = (trim($host) != '') ? $host : $this->host;
        $this->username = (trim($user) != '') ? $user : $this->username;
        $this->password = (trim($pass) != '') ? $pass : $this->password;
        $this->dbname = (trim($dbname) != '') ? $dbname : $this->dbname;
        $this->db_connect();

    }
    public function db_connect(){
    $this->dbconnection = mysqli_connect($this->host,$this->username,$this->password,$this->dbname) or die(mysqli_error($this->dbconnection));
    }

    public function select_db($dbname = ''){
        $this->dbname = (trim($dbname) != '') ? $dbname : $this->dbname;
        mysqli_select_db($this->dbconnection,$this->dbname) or die(mysqli_error($this->dbconnection));
    }
  
    public function query($query){
        $this->result = mysqli_query($this->dbconnection,$query) or die(mysqli_error($this->dbconnection)."\nquery: ".$query);
    }

    public function truncate($table){
        $que = "TRUNCATE TABLE {$table}";
        $this->query($que);
        if(mysqli_error($this->dbconnection)){
            return false;
        }
        return true;
    }

    public function select($table, $fields, $condition = false, $options = ""){
        $que = "SELECT $fields FROM $table";
        if(!empty($condition))
            $que .= " WHERE $condition";

        if(!empty($options))
            $que .= " {$options}";

        //echo $que;
        $this->query($que);
        $this->total = mysqli_num_rows($this->result);
    }

    public function get_data(){
        if($this->total){
            $row = array();
            if($this->total >= 1){
                while($rowOut = mysqli_fetch_assoc($this->result)){
                    $row[] = $rowOut;
                }
            }
            return $row;
        }else{
            return 0;
        }
    }
   
    public function update($table,$data,$condition = false){
        $que = "UPDATE $table SET $data";
        if(!empty($condition))
            $que .= " WHERE $condition";

        $this->query($que);

        if(mysqli_error($this->dbconnection)){
            return false;
        }
        return true;
    }
   
    public function db_close(){
        mysqli_close($this->dbconnection);
    }
   
    public function escapestring($string){
        return mysqli_real_escape_string($this->dbconnection,$string);
    }
   
    public function __destruct(){
        $this->db_close();
    }

}
