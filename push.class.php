<?php 


require_once "/home/dmp/dmp/scripts/preparation/daily_dose/mdp.constants.php";
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/db.class.php";

/*require_once "C:/xampp/htdocs/platform/ddose/mdp.constants.php";
require_once "C:/xampp/htdocs/platform/ddose/db.class.php";*/



ini_set('memory_limit', '-1');


class Push extends DB_Connect{

    private $has_data=0;
    //private $_incserviceid = array('subscribers', 'test_subscribers');
    public function __construct($host='',$user='',$pass='',$dbname=''){
    parent::__construct($host,$user,$pass,$dbname);
    }
    public function push_base_truncate(){
        return $this->truncate('push_base');
    }
    
    public function check_push_base($push_base_table){
        $sql    = "SELECT count(*) FROM " . MDP_CONSTANTS::DBCRM . "." ."{$push_base_table}";
        $result = $this->query($sql);
        $count = mysqli_fetch_row($this->result);
        return $count[0];
    }

    public function verify_push_base(){
        $sql = "SELECT count(*) FROM Dmp_Transaction.push_base";
        $result = $this->query($sql);
        $count = mysqli_fetch_row($this->result);
        return $count[0];
    }

    public function getMsgRepliesByKeyword($kw){
        $sql = "
            SELECT
            *
            FROM Dmp_Content.`keywords` kw
            LEFT JOIN Dmp_Content.`msg_replies` mr ON mr.keyword_id = kw.keyword_id
            WHERE
             kw.keyword='{$kw}'
            LIMIT 1;
        ";        
        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;        
    }

    public function kitin_subscriber($subtype){
        $sql = "SELECT s.subscriber_id, s.msisdn, s.subscription_type, s.status_id, s.is_charged, s.next_charge
                    FROM subscriber as s WHERE s.status_id IN(1,3,6)
                    AND s.subscription_type = {$subtype} AND s.next_charge <= '".date('Y-m-d')."'";
        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function get_delivered_result($service_id,$connection_id,$delivered = 0,$table = 'push_outbox'){
        $tbl = MDP_CONSTANTS::DBCRM . "." . $table;
        $flds = 'COUNT(*) AS ctr';
        $con = " delivered = {$delivered} AND  service_id = '{$service_id}' AND connection_id = '{$connection_id}' ";
        $opt = " LIMIT 1;";
        $this->select($tbl,$flds,$con,$opt); // query
        $data = $this->get_data(); // get data

        return $_delivered_count = $data[0]["ctr"];
        //echo "\nCOUNT of 0: ".$_delivered_count;

        //return $_delivered_count;
    }

    public function get_push_rebill_counter($service_id){
        $tbl = MDP_CONSTANTS::DBCRM . "." . "push_transaction_status";
        $flds = "COUNT(service_id) AS ctr";
        $con = "service_id = '{$service_id}' AND DATE(push_time) = DATE(NOW())";
        $this->select($tbl,$flds,$con,''); //query
        $data = $this->get_data(); // get data

        return $counter = $data[0]["ctr"];
        //echo "\nREBILL COUNT: ".$counter;

        //return $counter;
    }

    public function insert_push_trans_stats($push_id,$service_id,$push_based_count){
        $endtime=mktime(date("H")+8,date("i"),date("s"),date("m"),date("d"),date("Y"));
        $insert = "INSERT INTO
                        " . MDP_CONSTANTS::DBCRM . "." . "push_transaction_status (id,push_id,service_id,base_count,push_status,push_time)
                   VALUES
                        ('','{$push_id}','{$service_id}','{$push_based_count}','1','".gmdate('Y-m-d H:i:s', $endtime)."')";
        $this->query($insert);
        echo "\nUpdated push status - ". mysqli_affected_rows($this->dbconnection) ."\n";

    }

    // Start function tag content
    public function billed($service_id){

        $sql = "SELECT ls.msisdn from log_status_".date('md')." as ls LEFT JOIN log_out_".date('md')." as lo
                    ON ls.transid = lo.transid WHERE ls.status_code = 0 AND ls.service_id = {$service_id} 
                    AND date(ls.time_status) = '".date('Y-m-d')."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function getMsisdnSuccOrInsuff($service_id, $keyword){
        $sql = "SELECT ls.msisdn from log_status_".date('md')." as ls LEFT JOIN log_out_".date('md')." as lo
                    ON ls.transid = lo.transid WHERE (ls.status_code = 1 OR ls.status_code = 0 ) AND ls.service_id = {$service_id} 
                    #AND lo.keyword_id = {$keyword} 
                    AND date(ls.time_status) = '".date('Y-m-d')."'";
        echo $sql;
        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;        
    }

    public function billed_daily($service_id){

        $sql = "SELECT ls.msisdn from log_status_".date('md')." as ls LEFT JOIN log_out_".date('md')." as lo
                    ON ls.transid = lo.transid WHERE ls.status_code = 0 AND ls.service_id = {$service_id} 
                    AND lo.keyword_id = 0 AND date(ls.time_status) = '".date('Y-m-d')."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function billed_weekly($service_id){

        $sql = "SELECT ls.msisdn from log_status_".date('md')." as ls LEFT JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.status_code = 0 AND ls.service_id = {$service_id} 
            AND lo.keyword_id = 2 AND date(ls.time_status) = '".date('Y-m-d')."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function billed_monthly($service_id){

        $sql = "SELECT ls.msisdn from log_status_".date('md')." as ls LEFT JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.status_code = 0 AND ls.service_id = {$service_id} 
            AND lo.keyword_id = 3 AND date(ls.time_status) = '".date('Y-m-d')."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function schedule_content(){
        //$sql = "SELECT content_id FROM content WHERE is_content_active = 1 AND date(schedule) = '".date('Y-m-d')."'";
        //$sql = "SELECT content_id FROM content WHERE is_content_active = 1 AND date(schedule) IN ('2015-10-14','2015-10-15','2015-10-16')";
        $sql = "SELECT content_id FROM content WHERE is_content_active = 1 AND date(schedule) = '".date('Y-m-d', strtotime('+1day'))."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }

    public function schedule_content_zero(){

        //$sql = "SELECT content_id FROM content WHERE is_content_active = 1 AND content_type = 0 AND date(schedule) = '".date('Y-m-d')."'";
        $sql = "SELECT content_id FROM content WHERE is_content_active = 1 AND content_type = 0 AND date(schedule) = '".date('Y-m-d', strtotime('+1day'))."'";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }

    public function charged_sub(){
        $sql = "SELECT r.subscriber_id FROM registration r INNER JOIN subscriber s on r.msisdn = s.msisdn WHERE s.is_charged = 1";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }

    public function free_sub(){
        $sql = "SELECT r.subscriber_id FROM registration r WHERE r.freetrial > 0";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }

    public function free_sub_zero(){
        $sql = "SELECT r.subscriber_id FROM registration r WHERE r.freetrial = 0";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }

    public function expired_register(){
        $sql = "SELECT r.msisdn FROM registration r WHERE r.freetrial = 1";

        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }
    public function getMsisdnNonRegister(){
        $sql = "
            SELECT 
            Subs.msisdn
            FROM `subscriber` Subs
            LEFT JOIN `registration` Reg ON Reg.msisdn = Subs.msisdn
            WHERE
                Reg.msisdn IS NULL
                AND Subs.msisdn != 0
                
        ";
//AND Subs.msisdn IN (9159472517,9954730353) #comment this line for production
        $result = $this->query($sql);
        $data = array();

        if($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;

    }
    // End function tag content

    // Start function report

    public function total_transaction($date, $service_id, $cycle){

       $sql = "SELECT COUNT(ls.msisdn) from log_status_".date('md')." as ls INNER JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.service_id = {$service_id} 
            AND date(ls.time_status) = '{$date}' AND left(ls.transid,2) = {$cycle}";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }

    }

    public function total_billed($date, $service_id, $cycle){

        $sql = "SELECT COUNT(ls.msisdn) from log_status_".date('md')." as ls INNER JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.service_id = {$service_id} AND ls.status_code = 0
            AND date(ls.time_status) = '{$date}' AND left(ls.transid,2) = {$cycle}";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }
    }

    public function total_error($date, $service_id, $cycle){

        $sql = "SELECT COUNT(ls.msisdn) from log_status_".date('md')." as ls INNER JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.service_id = {$service_id} AND ls.status_code <> 0
            AND date(ls.time_status) = '{$date}' AND left(ls.transid,2) = {$cycle}";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }
    }

    public function total_insuff($date, $service_id, $cycle){

        $sql = "SELECT COUNT(ls.msisdn) from log_status_".date('md')." as ls INNER JOIN log_out_".date('md')." as lo
            ON ls.transid = lo.transid WHERE ls.service_id = {$service_id} 
            AND date(ls.time_status) = '{$date}' AND left(ls.transid,2) = {$cycle}
            AND ls.status_msg like '%Insuff%'";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }
    }

    public function opt_in($date, $type){

        $sql = "SELECT COUNT(subscriber_id) FROM subscriber_log
                WHERE status_id IN (1,3,6) AND subscription_type = {$type} AND date(logged_at) = '".$date."'";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }
    }

    public function opt_out($date, $type){

        $sql = "SELECT COUNT(subscriber_id) FROM subscriber_log
                WHERE status_id IN (2,4,5) AND subscription_type = {$type} AND date(logged_at) = '".$date."'";

        $result = $this->query($sql);
            
        if($this->result != NULL){
            $row = mysqli_fetch_row($this->result);
            return $row[0];
        } else {
            return 0;
        }
    }

    public function insert_transaction_report($date,$type,$cycle,$billed,$insuff,$error,$total){

        $insert = "INSERT INTO push_transaction (push_date,push_type,push_cycle,push_billed,error_insuff,error_other,total_transaction)
                    VALUES ('{$date}','{$type}','{$cycle}','{$billed}','{$insuff}','{$error}','{$total}')";
        
        $this->query($insert);
    }

    public function insert_subscription_report($date,$type,$optin,$optout){

        $insert = "INSERT INTO push_subscription (push_date,push_type,optin,optout)
                    VALUES ('{$date}','{$type}','{$optin}','{$optout}')";
        
        $this->query($insert);
    }
    // End function report


    public function get_omnibus_msisdn(){
        $sql    = "SELECT s.shortcode_id, s.msisdn, TIMESTAMPDIFF(DAY,MAX(l.logged_at),NOW()) AS 'days_inactive'
                    FROM subscriber AS s
                    JOIN subscriber_log AS l
                    ON s.subscriber_id = l.subscriber_id
                    WHERE s.status_id = l.status_id AND s.status_id IN(1,3,6)
                    GROUP BY l.subscriber_id
                    HAVING days_inactive > 7 limit 1000000";
                    
        $result = $this->query($sql);
        $data = array();
        
        if ($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function get_omnibus_pushoutbox(){
        $sql    = "SELECT * FROM push_base";
                    
        $result = $this->query($sql);
        $data = array();
        
        if ($this->result != NULL){
            while($row = mysqli_fetch_assoc($this->result)){
                $data[] = $row;
            }
        }
        return $data;
    }

    
    
    public function __destruct(){}
}

?>
