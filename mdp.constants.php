<?php
/**
 * MDP CONSTANTS
 *
 * @package ENGINE
 * @author Caesare M. Morata
 * @version 0.3.1
 * @copyright Copyright (c) 2008-2010 Entertainment Gateway Group (http://www.egg.ph)
 *
 * @name mdp.constants.php
 *
 */

class MDP_CONSTANTS{

    const APP_NAME  = "MDP";
    const APP_FULL_NAME  = "Mobile Delivery Platform";
    const APP_VERSION  = "3.0";
    const SOCKET_RECONNECT_TIMEOUT = 60;    
    const MSISDN_LENGTH = 10;
    const DEFAULT_SCENE = 0;    

    # folder
    const LIBRARY   = "library/";
    const GATEWAY   = "gateway/";
    const SCRIPTS   = "scripts/";
    const ENGINE   = "engine/";

    # db connections
    const CONCMS    = "cms";
    const CONCRM    = "crm";
    const CONGW    = "gw";
    #content connections
    const CMSIP     = "10.176.160.187";
    const CMSUSER   = "dmp_cms";
    const CMSPASS   = "s!hee366nacms";
    #transaction connections
    const CRMIP     = "10.176.160.187";
    const CRMUSER   = "dmp_crm";
    const CRMPASS   = "3g6s!heenacrm";
    #gateway connections
    const GWIP     = "10.176.160.187";
    const GWUSER   = "dmp_gw";
    const GWPASS   = "s!heg6eegwna";

    #database
    const DBCENTER  = "Dmp_CMS";   
    const DBCMS     = "Dmp_Content";
    const DBCRM     = "Dmp_Transaction";
    const DBOMNI     = "Dmp_Omnibus";
    const DBDDOSE   = "Daily_Dose";

    #logging tables
    const INBOX     = "log_in";
    const TMPOUTBOX = "pull_outbox";
    const OUTBOX    = "log_out";
    const STATUSBOX = "log_status"; 
    # content tables
    const T_CONNECTIONS = "connections";
    const T_SHORTCODES  = "shortcodes";
    const T_KEYWORDS    = "keywords";
    const T_SERVICES    = "services";
    const T_PUSHSERVICES= "push_services";    
    const T_KEYWORDSETS = "keyword_sets";
    const T_SCRIPTS     = "scripts";
    const T_SCENES      = "scenes";
    const T_SCENESETS   = "scene_sets";
    const T_REPLIES     = "msg_replies";
    const T_TARIFFS     = "tariffs";
    const T_SUBSCRIBERS = "subscribers";
    const T_CONTENTS    = "contents";
    const T_CENTER      = "mdp_center";
    const T_TICKETS     = "tickets"; 
    const T_REGISTRATION     = "registration";  
    const T_MCN		= "mcn";
    const T_MOBID = "mobid";

    const T_TELCOS = 'telco';
    const T_PREFIX = 'telco_prefix';
    const T_PLAN = 'plan';
    const T_PLAN_PREFIX = 'plan_prefix';

    #wacoal headquarters number
    const W_HQ_NUM	= "9178907621";
//    const W_HQ_NUM	= "9173835207";	
    #log files
    const L_APPLICATION = "mdp_log"; #gateway/application
    const L_PROCESS     = "mdp_process"; #processing
    const L_ERROR       = "mdp_error"; #error logs
    //const L_WHITELIST   = "mdp_whitelist"; #whitelist number/testing
    //const L_ACCESS      = "mdp_access"; #client/3rd party log

    #mdp_transactions
    const T_TAQ = "tq";
    const T_IMP = "impeachment";
    const T_DONATE = "servicename_donate";
}
?>
