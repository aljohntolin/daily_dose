<?php

require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.config.php";
require_once "/home/dmp/dmp/scripts/preparation/daily_dose/push.class.php";

ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');
date_default_timezone_set("Asia/Manila");

$date = exec('date');
$date = new DateTime($date);
$service_id = $argv[1];
if($service_id == 57228){
	$subtype = 1;
} else if($service_id == 57229){
	$subtype = 2;
} else if($service_id == 57230){
	$subtype = 3;
} else {
	echo "\nNot valid service id for daily dose\n";
}

// 5723 - daily, 5724 - weekly, 5725 - monthly

//@$keyword_id = $argv[2];

/*if($keyword_id == 1){
	$sub_type = "Daily";
}
else if($keyword_id == 2){
	$sub_type = "Weekly";
}
else if($keyword_id == 3){
	$sub_type = "Monthly";
} */

if($service_id == NULL){
	echo "\nERROR: SERVICE ID is required as parameter.\n";
	exit;
} /*else if($keyword_id == NULL){
	echo "\nERROR: KEYWORD ID is required as parameter.\n";
	exit;
} else if($keyword_id > 3 || $keyword_id < 1) {
	echo "\nERROR: KEYWORD ID should be 1, 2 or 3 only.\n";
	echo "1 = daily subscription\n";
	echo "2 = weekly subscription\n";
	echo "3 = monthly subscription\n";
	exit;
}*/

echo "\nPrepare for PUSH";
$push = new Push('10.208.152.24','dmp_push','DPus4832hSxMpLp','Daily_Dose'); // KITIN
echo "\nConnected to KITIN.";
//echo "\nQuerying daily dose subscriber base..";
//$subscriber = $push->kitin_subscriber(); // KITIN Subscriber Base

$push2 = new Push('10.176.160.187','dmp_push','sh3ena29I0','Dmp_Transaction'); // Sheena
#$push2 = new Push('10.178.20.75','dmp_push','gDg3mPush36g','Dmp_Transaction'); // MELANIE
echo "\nConnected to DB.";
echo "\nChecking push base on Melanie...\n";
$push_base_count = $push2->check_push_base(PUSHBASE); 

if($push_base_count){
	echo "\nERROR: Cannot prepare, push base is not yet empty. Please check push base and try again later.\n";
	exit;
}

//Get Push Details from Push Transactions
$push2->select(MDP_CONSTANTS::DBCRM . '.push_transaction'
			, '*' 
			,"scheduled = 1 AND active = 1 AND pr_status = 1 AND service_id = '{$service_id}'",'LIMIT 1'); // query

$push_data = $push2->get_data(); //Put data from push transaction to $push_data array.
$push_data = $push_data[0];//Initially, The $push_data can be accessed like so: $push_data[0]['project_id']. This line will put the data out of the [0] index and then transferred to the $push_data. Now you can access the push_data like so: $push_data['project_id'];
print_r($push_data);
echo "\nSubscription type: ".$sub_type;
echo "\nDone getting the parameters needed from push_transaction table\n";

//Checking push_outbox And The Current Cycle The Push Process Is On
$DELIVERED_69_COUNT = $push2->get_delivered_result($service_id,$push_data['connection_id'],69,'push_outbox'); //TESTING: set to 0
$DELIVERED_7_COUNT = $push2->get_delivered_result($service_id,$push_data['connection_id'],7,'push_outbox'); // TESTING: set to 0
$COUNTER = $push2->get_push_rebill_counter($service_id); //TESTING: set to  0
$PREVCOUNTER = $COUNTER - 1;

echo "\nPush_outbox Counts and Push Cycle Count";
echo "\nDELIVER 7 COUNT: ".$DELIVERED_7_COUNT;
echo "\nDELIVER 69 COUNT: ".$DELIVERED_69_COUNT;
echo "\nCOUNTER COUNT: ".$COUNTER;
echo "\nPREVCOUNTER COUNT: ".$PREVCOUNTER;  
//End Checking push_outbox And The Current Cycle The Push Process Is On

//Validationg push_outbox content.
if($DELIVERED_7_COUNT == 0 && $DELIVERED_69_COUNT ==0){

	if($COUNTER <= 0) {
		$counter = '99';
		echo "\n\nStart 1st PUSH\n";

		echo "Querying subscriber base on KITIN..\n";
		$subscriber = $push->kitin_subscriber($subtype);

		foreach($subscriber as $pushB) {
			$push2->query("INSERT INTO ".MDP_CONSTANTS::DBCRM . "." . "push_base (msisdn) VALUES('".$pushB['msisdn']."')");
		}

		echo "Inserted to PUSH BASE.\n";
		$insert = "INSERT INTO " . MDP_CONSTANTS::DBCRM . "." . "push_outbox
                    (
	                	transid,
	                	msisdn,
	                	project_id,
	                	service_id,
	                	tariff_id,
	                	billing,
	                	connection_id,
	                	shortcode_id,
	                	source,
	                	keyword_id,
	                	alias,
	                	content_id,
	                	scene_id,
	                	delivered,
	                	ptype,
	                	ctype,
	                	mms,
	                	drm,
	                	telco_id,
	                	msg_index,
	                	rcvd_transid,
	                	dcs,
	                	content
                	) 
				
                    	SELECT 
                    	CONCAT('99{$service_id}',msisdn,UNIX_TIMESTAMP(NOW()),FLOOR(10 + (RAND() * 90)),'111') AS transid, 
	                    msisdn,
	                    '{$push_data['project_id']}' AS project_id,
	                    {$service_id},
	                    {$push_data['tariff_id']} AS tariff_id,
	                    '{$push_data['billing']}',
	                    '{$push_data['connection_id']}', 
	                    '{$push_data['shortcode_id']}', 
	                    '{$push_data['source']}',
	                    '{$push_data['keyword_id']}',
	                    '{$push_data['alias']}', 
	                    '{$push_data['content_id']}', 
	                    '{$push_data['scene_id']}',
	                    '69', 
	                    1,
	                    '{$push_data['ctype']}', 
	                    '{$push_data['mms']}', 
	                    '{$push_data['drm']}', 
	                    '{$push_data['telco_id']}', 
	                    '{$push_data['msg_index']}',
	                    '{$push_data['rcvd_transid']}', 
	                    '{$push_data['dcs']}', 
	                    ''
	                    FROM " . MDP_CONSTANTS::DBCRM . "." . "push_base
	                ";
            //echo $insert."\n\n";
	        echo "Inserted to Push Outbox.\n";
	        $push2->query($insert);
	}
	else { //REBILL

		 if ($COUNTER > 0 && $COUNTER < 3) {               
            	if($COUNTER == 1){ // 1st Rebill

            		echo "\nStart REBILL : 1";
            		echo "\nInserting Push Base..";
            		$push2->query("INSERT INTO " .MDP_CONSTANTS::DBCRM . "." . "push_base (msisdn) 
	    				SELECT DISTINCT ls.msisdn
						FROM Dmp_Transaction.log_status_".$date->format('md')." AS ls
						WHERE ls.service_id = {$service_id} AND ls.msg_index = 1 AND ls.status_code <> 0 
						AND LEFT(ls.transid,2) = '99'");
            		echo "\nInserted to Push Base.";

            	}else{ // 2nd Rebill and so on..
            		
            		echo "\nStart REBILL : ".$COUNTER;
            		echo "\nInserting Push Base..";
            		$push2->query("INSERT INTO " .MDP_CONSTANTS::DBCRM . "." . "push_base (msisdn) 
	    				SELECT DISTINCT ls.msisdn
						FROM Dmp_Transaction.log_status_".$date->format('md')." AS ls
						WHERE ls.service_id = {$service_id} AND ls.msg_index = 1 AND ls.status_code <> 0 
						AND LEFT(ls.transid,2) = '0{$PREVCOUNTER}'");
            		echo "\nInserted to Push Base.";
            	}

            	echo "\nInserting Push Outbox..";
			    $counter = '0'.$COUNTER;                         
            	$insert = "INSERT INTO " . MDP_CONSTANTS::DBCRM . "." . "push_outbox 
                    (
	                	transid,
	                	msisdn,
	                	project_id,
	                	service_id,
	                	tariff_id,
	                	billing,
	                	connection_id,
	                	shortcode_id,
	                	source,
	                	keyword_id,
	                	alias,
	                	content_id,
	                	scene_id,
	                	delivered,
	                	ptype,
	                	ctype,
	                	mms,
	                	drm,
	                	telco_id,
	                	msg_index,
	                	rcvd_transid,
	                	dcs,
	                	content
                	) 
					
                   
                    	SELECT 
                    	CONCAT('{$counter}{$service_id}',msisdn,UNIX_TIMESTAMP(NOW()),FLOOR(10 + (RAND() * 90)),'111') AS transid, 
	                    msisdn,
	                    '{$push_data['project_id']}' AS project_id,
	                    {$service_id},
	                    {$push_data['tariff_id']} AS tariff_id,
	                    '{$push_data['billing']}',
	                    '{$push_data['connection_id']}', 
	                    '{$push_data['shortcode_id']}', 
	                    '{$push_data['source']}',
	                    '{$push_data['keyword_id']}',
	                    '{$push_data['alias']}', 
	                    '{$push_data['content_id']}', 
	                    '{$push_data['scene_id']}',
	                    '69', 
	                    1,
	                    '{$push_data['ctype']}', 
	                    '{$push_data['mms']}', 
	                    '{$push_data['drm']}', 
	                    '{$push_data['telco_id']}', 
	                    '{$push_data['msg_index']}',
	                    '{$push_data['rcvd_transid']}', 
	                    '{$push_data['dcs']}', 
	                    ''
	                    FROM " . MDP_CONSTANTS::DBCRM . "." . "push_base
	                ";
	            //echo $insert."\n\n";
	            $push2->query($insert);
            }
         else {
         	echo "Maximum only of 3 rebill.\n";
         }
            echo "\nInserted to Push Outbox.\n";
	}
}
else {
	echo "\nDelivered 7 and 69 is not yet ZERO.";
	exit;
}

echo "Completed.";


//$push2->insert_push_trans_stats($push_data['push_id'], $service_id, mysqli_affected_rows($push2->dbconnection));  

if($push2->push_base_truncate(PUSH_BASE)){
    echo "\nPUSH BASE TRUNCATED!!!\n";
}else{
	echo "\n\n ERROR: truncate not completed. mysql_error ....\n\n";
    exit;
}